Host a VIGOR DAC member client

Custodians are encouraged to host an instance of the DAC member client (UI), so we have many sites at the same time.

Can customize your instance with your custodian branding.

#ssh for gitlab https://docs.gitlab.com/ee/ssh/
git clone git@gitlab.com:vigorstablecoin/eosio-statereceiver.git
cd eosio-statereceiver
git reset --hard ccc682ca98d999ab568d4cbe4c45fdc65faca4dc

cd ~
git clone git@gitlab.com:vigorstablecoin/eosdac-api.git
cd ~/eosdac-api
git reset --hard de3701ddd5bdf1b48895b8d614fdadc75cd1101d

git clone git@gitlab.com:vigorstablecoin/eosdac-client.git
cd ~/eosdac-client
git reset --hard d2091064f2ccd7296f21a7644820a8fd6f183db5

cd ~/eosdac-client/src
git clone --recursive git@gitlab.com:vigorstablecoin/eosdac-client-extension.git extensions
cd extensions

the filler scrapes the blockchain for transactions related to list of designated contracts, it fills a mongo db locally with help of a processor (and amqp for distributed processing), and provides an API for the UI client to access state history. The UI uses quasar/vue and a ton of nodejs to populate the UI with relevant state from the API and pushes user transactions to the blockchain (scatter api is used, which is compatible with any wallet that uses that protocol)

DAC Client Install Notes 20200113

#linux ubuntu 18.04
# yarn and node
curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -
echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list
sudo apt-get update && sudo apt-get install yarn
sudo apt remove cmdtest
cd ~
curl -sL https://deb.nodesource.com/setup_10.x -o nodesource_setup.sh
sudo bash nodesource_setup.sh
sudo apt-get install -y nodejs
nodejs -v

#rabbitmq/erlang install
echo "deb http://www.rabbitmq.com/debian/ testing main"  | sudo tee  /etc/apt/sources.list.d/rabbitmq.list > /dev/null
wget https://www.rabbitmq.com/rabbitmq-signing-key-public.asc
sudo apt-key add rabbitmq-signing-key-public.asc
sudo apt-get update
sudo apt-get install rabbitmq-server -y
sudo service rabbitmq-server start
sudo rabbitmq-plugins enable rabbitmq_management
sudo service rabbitmq-server restart

#mongodb install, it's needed by the processor (not the filler)
sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 68818C72E52529D4
sudo echo "deb http://repo.mongodb.org/apt/ubuntu bionic/mongodb-org/4.0 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-4.0.list
sudo apt-get update
sudo apt-get install -y mongodb-org
sudo systemctl start mongod
server_names_hash_bucket_size 64;
sudo systemctl enable mongod
sudo netstat -plntu
apt-get install mongodb-server
sudo service mongodb restart

#eosio-statereceiver
cd ~/eosio-statereceiver
sudo npm install

#eosdac-api
cd ~/eosdac-api
sudo npm install
sudo npm i -g pm2
# the first time, replay the filler starting at the block number the dac went live using -s
#when started in replay mode (-r) it will create the block range jobs and then individual processes will run each one
#if the filler stops, you can restart it with -p which will continue from where it left off
# (-s block number, -p last block)
#CONFIG=mainnet ./eosdac-filler.js -r -s 99972000
CONFIG=mainnet ./eosdac-filler.js -r -s 56700000
# once the filler is caught up to latest block, ctrl-c to stop the filler and run pm2 to kick off filler/processor
pm2 start
pm2 status
pm2 show
#pm2 start
#pm2 delete all

#then check the mongo database by typing mongo and you should get a shell, ctrl+d to exit
#show dbs
#use dacmain
#db.dropDatabase()
#use dacmain
#show collections
#db.multisigs.find()
#db.multisigs.find({},field:true,field:true)
#db.multisigs.find({proposal_name:"dlwqnweidimc"})
#db.multisigs.find({action.account:"dacmultisig1",action.name:"proposed"})
#https://stackoverflow.com/questions/16619598/sync-mongodb-via-ssh

# watchers
# can refresh all the msigs:
# you can let it run until it has got all the msigs, then stop it
CONFIG=mainnet node watchers/replay.js

#for jungle: eosio.msig in eosdac-filler/jungle.config.js should be eosiomsigold It’s the old version of eosio.msig which is deployed on jungle

#not needed but you might want to enable the management plugin, to clear the queue, multithreading
sudo rabbitmq-plugins enable rabbitmq-management
sudo rabbitmq-plugins enable rabbitmq-shovel
sudo rabbitmq-plugins enable rabbitmq-shovel-management
sudo lsof -i -P -n | grep LISTEN
sudo rabbitmqctl list_permissions
sudo rabbitmqctl set_user_tags ubuntu administrator
sudo rabbitmqctl set_permissions -p /ubuntu ".*" ".*" ".*"
http://api-dac.vigor.ai:15672/ # see the proccess list in the queue, can purge them if desired
# the filler will populate the queue in rabbitmq, then filler will run each process in the queue and write to mongodb
$ cat /etc/rabbitmq/enabled_plugins
[rabbitmq_management,rabbitmq_shovel,rabbitmq_shovel_management].
https://www.rabbitmq.com/passwords.html


#production web server
sudo apt-get install nginx
sudo apt-get update
sudo apt-get install software-properties-common
sudo add-apt-repository universe
sudo add-apt-repository ppa:certbot/certbot
sudo apt-get update
sudo apt install certbot
sudo apt install python-certbot-nginx

#serve up https://api-dac.vigor.ai
sudo mkdir -p /var/www/api-dac.vigor.ai/dist/spa-mat
#put a file in there, index.html
sudo chmod -R 755 /var/www/api-dac.vigor.ai
#create a config file: 
/etc/nginx/sites-available/api-dac.vigor.ai
#uncomment a line in /etc/nginx/nginx.conf
server_names_hash_bucket_size 64;
sudo ln -s /etc/nginx/sites-available/api-dac.vigor.ai /etc/nginx/sites-enabled/
sudo rm /etc/nginx/sites-enabled/default
sudo certbot --nginx -d api-dac.vigor.ai

#serve up https://vigor.ai
sudo mkdir -p /var/www/vigor.ai/dist/spa-mat
sudo chmod -R 755 /var/www/.vigor.ai
#create a config file: 
/etc/nginx/sites-available/vigor.ai
#uncomment a line in /etc/nginx/nginx.conf
sudo ln -s /etc/nginx/sites-available/vigor.ai /etc/nginx/sites-enabled/
sudo rm /etc/nginx/sites-enabled/default

#ports to open on the server
SSH	TCP	22	
HTTP	TCP	80	
HTTPS	TCP	443	
Custom	TCP	8382 (~/eosdac-api/ecosystem.config.js)
Custom	TCP	15672 (rabbitmq management plugin)
# dns A records for vigor.ai: point all these to the staic IP of the server: @ www and api-dac


#build the client
cd ~/eosdac-client
yarn
# run DAC client locally for dev http://localhost:8080
DEFAULT_NETWORK=mainnet yarn dev
# run DAC client on nginx producation server
DEFAULT_NETWORK=mainnet yarn build
sudo cp -r ~/eosdac-client/dist /var/www/vigor.ai/.
sudo certbot --nginx -d vigor.ai -d www.vigor.ai -d vig.ai -d www.vig.ai

sudo nginx -t
sudo systemctl restart nginx

#sudo systemctl stop nginx
#sudo systemctl start nginx
#sudo systemctl reload nginx
#sudo systemctl disable nginx
#sudo systemctl enable nginx

# vigor whitepaper pdfs can be copied to the nginx server folder
sudo cp ~/eosdac-api/src/extensions/branding/pdfs/*.pdf /var/www/vigor.ai/dist/spa-mat/.

# config files
#cd ~/eosdac-api
#mainnet.config.js
#ecosystem.js
#cd ~/eosdac-client/src/extensions/static/config
#config.mainnet.json
#eosdac endpoint
#https://eu.eosdac.io


###################
# old notes       #
###################

#for state history
#ws://ex2.eosdac.io:8080
#ws://as1.eosdac.io:8080

docs url?
https://api-jungle.eosdac.io/v1/eosdac/docs/index.html


#small patch for buttons on mainpage, add these lines:
src/plugins/config-loader.js
case "external":
        return this.configFile.external;

put these pdf files into dist/spa-mat/
wget https://vig.ai/vigorstablecoin.pdf
wget https://vig.ai/vigor.pdf

change this to false to enable claim all
https://github.com/eosdac/eosdac-client/blob/dev/src/pages/custodian/my-payments.vue#L93

refresh tweak
change the 250 to 750 directly in your client source
https://github.com/eosdac/eosdac-client/blob/dev/src/store/user/actions.js#L237 

change default number of candidates on main page to 48
https://github.com/eosdac/eosdac-client/blob/master/src/pages/vote-custodians.vue#L289

launch dac contract and unlock.
vi ecosystem.config.js and comment out eosdac-filler-mainnet (will uncomment later)
CONFIG=mainnet ./eosdac-filler.js -r -s 56700000
pm2 start (runs the processor and api)
http://api-dac.vigor.ai:15672/ # watch and see the filler populate the queue, can purge if you need, the propcessor works on each process and writes to mongod
#mongo
#use dacmain
#show collection
# after the filler/processor syncs to LIB last irrev block, then 
# uncomment eosdac-filler-mainnet in ecosystem.config.js
# kill the filler
# pm2 delete all
# pm2 start
# pm2 show

#ws://as1.eosdac.io:8080

which state history endpoint are you using?
our as1 server recently died and i have restored it from 1.8 snapshots which means it is not compatible with the eosio-statereceiver
there is a 1.8 tag on eosio-statereceiver, we have another <1.8 state history on ex2
i think it is best that you update though, we dont have long before we all have to be on 1.8
f
stuffed shirt | vig.aiadmin
ws://as1.eosdac.io:8080
ws://ex1.eosdac.io:8080
ws://ex2.eosdac.io:8080

-p is used to add extra processors so you can use many machines to do a replay
it only sends blockranges when you start with -r
not using -r means serial 
CONFIG=mainnet ./eosdac-filler.js -s 91738304

Michael Yeates
yeah, thats why
go to the filler/eosio-statehistory folder
git pull, then checkout the 1.8 tag

stuffed shirt | vig.aiadmin
right ok

Michael Yeates
then run npm there and also in the filler folder
then stop both pm2 processes (filler and processor)
you should be able to restart the filler and it will pick up where it left off
check the logs are happy and sending to the queue, then start the processor and check logs

stuffed shirt | vig.aiadmin
ok

# eosio.msig approval table...suddenly it has started using approvals2 table, which has a differnt abi
# here is the fix for that:
https://github.com/vigorstablecoin/eosdac-api-extension

use these two customized files:
eosdac-api/watchers/multisig_proposals.js
eosdac-api/handlers/delta-handler.js

then from the eosdac-api directory:
pm2 delete all
pm2 start
CONFIG=mainnet node ./watchers/replay.js

pm2 delete all
use this history node
ws://state.eosdac.io:8080
inside this config file
eosdac-api/config.mainnet.js
CONFIG=mainnet ./eosdac-filler.js -s 91738304 (or last block you have)
after that finishes, fully synced to the latest block
pm2 start
CONFIG=mainnet ./watchers/replay.js